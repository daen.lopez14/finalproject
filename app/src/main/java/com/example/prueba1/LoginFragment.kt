package com.example.prueba1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.prueba1.auth.AuthenticationViewModel
import com.example.prueba1.databinding.FragmentLoginBinding

/**
 * A simple [Fragment] subclass.
 * Use the [LoginFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginFragment : Fragment() {

    private val authenticationViewModel by viewModels<AuthenticationViewModel>()
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginBtn.setOnClickListener {
            authenticationViewModel.getRequestToken()
            authenticationViewModel.requestToken.observe(viewLifecycleOwner, Observer { tokenResource
                when(tokenResource)
            })
            LoginExternal2()
        }
    }

    fun LoginExternal2() {
        Toast.makeText(context, "button clicked", Toast.LENGTH_SHORT).show()
        val action = LoginFragmentDirections.actionBlankFragment1ToWebview()
        findNavController().navigate(action)
    }

    fun AuthorizationSession(){
        val Auth: Auth
    }

}