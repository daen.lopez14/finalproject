package com.example.prueba1.auth

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.work.impl.Schedulers
import com.example.data.network.ApiManager
import com.example.data.network.utils.Resource
import com.example.data.network.auth.CreateSessionRequest
import com.example.data.network.utils.SessionIdInterceptor
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers.io
import java.io.IOException
import java.util.concurrent.TimeoutException

class AuthenticationViewModel(sharedPreferences: SharedPreferences,
                              private val sessionIdInterceptor: SessionIdInterceptor,
                              private val apiManager: ApiManager
) : ViewModel() {

    private val _requestToken = MutableLiveData<Resource<String>>()
    private val _sessionId = MutableLiveData<Resource<String>>()
    private val _authSuccess = SingleLiveEvent<Boolean>()
    private val _message = SingleLiveEvent<String>()
    private val compositeDisposable = CompositeDisposable()

    val requestToken: LiveData<Resource<String>>
        get() = _requestToken

    val authSuccess: LiveData<Boolean>
        get() = _authSuccess

    val message: LiveData<String>
        get() = _message

    fun getRequestToken() {
        apiManager
            .getRequestToken()
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { token -> _requestToken.postValue(token) },
                onError = { error -> handleError(error, "get-request-token")}
            )
            .disposeWith(compositeDisposable)
    }

    fun createSession(request: CreateSessionRequest) {
        apiManager
            .createSession(request)
            .subscribeOn(Schedulers.io())
            .doOnSuccess {
                this.getAccountDetails()
            }
            .subscribeBy(
                onSuccess = { sessionIdResource ->
                    _sessionId.postValue(sessionIdResource)
                    if (sessionIdResource is Resource.Success) {
                        sessionIdPref = sessionIdResource.data
                        isAuthenticatedPref = true
                        _authSuccess.postValue(true)
                        setNewSessionIdToInterceptor(sessionIdResource.data)
                    }
                },
                onError = { error -> handleError(error, "create-session")}
            )
            .disposeWith(compositeDisposable)
    }

    fun getAccountDetails() {
        apiManager
            .getAccountDetails()
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { accountDetails ->
                    _accountDetails.postValue(accountDetails)
                    if (accountDetails is Resource.Success) {
                        accountIdPref = accountDetails.data.id
                    }
                },
                onError = { error -> handleError(error, "get-account-details") }
            )
            .disposeWith(compositeDisposable)
    }

    fun setNewSessionIdToInterceptor(newId: String) {
        sessionIdInterceptor.setSessionId(newId)
    }

    private fun handleError(error: Throwable, caller: String) {
        error.localizedMessage?.let {
            log("ERROR $caller -> $it")
        } ?: log("ERROR $caller ->")
            .also {
                error.printStackTrace()
            }
        when (error) {
            is IOException -> _message.postValue("Please check your internet connection")
            is TimeoutException -> _message.postValue("Request timed out")
            else -> _message.postValue("An error occurred")
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}