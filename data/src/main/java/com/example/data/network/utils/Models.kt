package com.example.data.network.utils

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class GeneralMovieResponse(
    @SerializedName(value = "adult") val isAdultMovie: Boolean,
    @SerializedName(value = "poster_path") var posterPath: String?,
    @SerializedName(value = "overview") val overview: String,
    @SerializedName(value = "release_date") var releaseDate: Date,
    @SerializedName(value = "genre_ids") val genreIds: List<Int>,
    @SerializedName(value = "id") val id: Int,
    @SerializedName(value = "original_title") val originalTitle: String,
    @SerializedName(value = "original_language") val originalLanguage: String,
    @SerializedName(value = "title") val title: String,
    @SerializedName(value = "backdrop_path") val backdropPath: String?,
    @SerializedName(value = "popularity") val popularity: Double,
    @SerializedName(value = "vote_count") val voteCount: Int,
    @SerializedName(value = "video") val video: Boolean,
    @SerializedName(value = "vote_average") var voteAverage: Double
) : Parcelable

@Parcelize
data class ErrorResponse(
    @SerializedName(value = "status_message") val statusMessage: String,
    @SerializedName(value = "status_code") val statusCode: Int
) : Parcelable


