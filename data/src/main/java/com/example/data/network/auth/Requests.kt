package com.example.data.network.auth

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreateSessionRequest(
    @SerializedName(value="request_token")val requestToken: String): Parcelable

@Parcelize
data class DeleteSessionRequest(
    @SerializedName(value="session_id") val sessionId: String): Parcelable