package com.example.data.network.modules


import androidx.room.Room
import com.example.data.R
import com.example.data.network.ApiManager
import com.example.data.network.auth.AuthenticationService
import com.haroldadmin.cnradapter.NetworkResponseAdapterFactory
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

val retrofitModule = module {

    single { (sessionId: String) -> SessionIdInterceptor(sessionId) }

    single { (apiKey: String) -> ApiKeyInterceptor(apiKey) }

    single { (loggingLevel: HttpLoggingInterceptor.Level) ->
        HttpLoggingInterceptor().apply {
            level = loggingLevel
        }
    }

    single {
        Retrofit.Builder()
            .client(get())
            .addCallAdapterFactory(get<NetworkResponseAdapterFactory>())
            .addConverterFactory(get<GsonConverterFactory>())
            .baseUrl("https://api.themoviedb.org/3/")
            .build()
    }
}

val databaseModule = module {

    single {
        Room.databaseBuilder(
            androidContext(),
            MovieDBDatabase::class.java,
            androidContext().getString(R.string.app_name)
        ).build()
    }
}

val apiModule = module {
    single { get<Retrofit>().create(AuthenticationService::class.java) }
    single { ApiManager(get(), get()) }
}

val repositoryModule = module {
    single { get<MovieDBDatabase>().moviesDao() }
    single { get<MovieDBDatabase>().actorsDao() }
    single { get<MovieDBDatabase>().collectionsDao() }
    single { LocalMoviesSource(get(), get()) }
    single { RemoteMoviesSource(get(), get(), get()) }
    single { MoviesRepository(get(), get()) }
}