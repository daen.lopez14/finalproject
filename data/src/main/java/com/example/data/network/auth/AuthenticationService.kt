package com.example.data.network.auth

import com.example.data.network.utils.ErrorResponse
import com.haroldadmin.cnradapter.NetworkResponse
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthenticationService {


    @GET("authentication/token/new")
    fun getRequestToken(): Deferred<NetworkResponse<RequestTokenResponse, ErrorResponse>>

    @POST("authentication/session/new")
    fun createNewSession(@Body request: CreateSessionRequest): Deferred<NetworkResponse<CreateSessionResponse, ErrorResponse>>

    @DELETE("authentication/session")
    fun deleteSession(@Body request: DeleteSessionRequest): Deferred<NetworkResponse<DeleteSessionRequest, ErrorResponse>>
}