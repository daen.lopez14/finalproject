package com.example.data.network.auth

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestTokenResponse(
    @SerializedName(value = "success") val success: Boolean,
    @SerializedName(value = "expires_at") val expiresAt: String,
    @SerializedName(value = "request_token") val requestToken: String
) : Parcelable

@Parcelize
data class CreateSessionResponse(
    @SerializedName(value = "success") val success: Boolean,
    @SerializedName(value = "session_id") val sessionId: String
) : Parcelable

@Parcelize
data class GuestSessionResponse(
    @SerializedName(value = "success") val success: Boolean,
    @SerializedName(value = "guest_session_id") val id: String,
    @SerializedName(value = "expires_at") val expiresAt: String
) : Parcelable